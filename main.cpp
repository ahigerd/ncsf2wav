#include <iostream>
#include "sseqplayer/XSFPlayer_NCSF.h"
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include "dumpinfo.h"

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>

#define MAIN wmain
#define cerr wcerr
#define _T(x) L##x
#define A_TO_I(x) _wtoi(x)
#define A_TO_D(x) std::wcstod(x, nullptr)
using string = std::wstring;
using oschar = wchar_t;
#else
#define MAIN main
#define _T(x) x
#define A_TO_I(x) std::atoi(x)
#define A_TO_D(x) std::strtod(x, nullptr)
using string = std::string;
using oschar = char;
#endif

static std::vector<uint8_t> littleEndian(uint32_t value) {
  return std::vector<uint8_t>({
    uint8_t(value),
    uint8_t(value >> 8),
    uint8_t(value >> 16),
    uint8_t(value >> 24)
  });
}

static double getMultiplier(const string& arg) {
  double mult = A_TO_D(arg.c_str());
  if (arg[arg.size() - 1] == '%') {
    mult /= 100.0;
  }
  return mult;
}

int MAIN(int argc, oschar** argv) {
  std::vector<string> args;
  for (int i = 1; i < argc; i++) {
    args.push_back(argv[i]);
  }

  bool unexpected = false;
  bool skipSilence = true;
  unsigned int sampleRate = 32728;
  double speedFactor = 1.0, tempoFactor = 1.0, preampFactor = 1.0;
  unsigned int limitSeconds = 0;
  Interpolation interpMode = INTERPOLATION_NONE;
  string inputFilename, outputFilename;
  std::vector<int> mutes, solos;
  bool dumpHtml = false;
  for (unsigned int i = 0; i < args.size(); i++) {
    if (args[i] == _T("--rate")) {
      ++i;
      sampleRate = A_TO_I(args[i].c_str());
    } else if (args[i] == _T("--length")) {
      ++i;
      limitSeconds = A_TO_I(args[i].c_str());
    } else if (args[i] == _T("--speed")) {
      speedFactor = getMultiplier(args[++i]);
    } else if (args[i] == _T("--tempo")) {
      tempoFactor = getMultiplier(args[++i]);
    } else if (args[i] == _T("--preamp")) {
      preampFactor = getMultiplier(args[++i]);
    } else if (args[i] == _T("--info")) {
      dumpHtml = true;
    } else if (args[i] == _T("--interp")) {
      ++i;
      if (args[i] == _T("linear")) {
        interpMode = INTERPOLATION_LINEAR;
      } else if (args[i] == _T("l4")) {
        interpMode = INTERPOLATION_4POINTLEGRANGE;
      } else if (args[i] == _T("l6")) {
        interpMode = INTERPOLATION_6POINTLEGRANGE;
      } else if (args[i] == _T("sinc")) {
        interpMode = INTERPOLATION_SINC;
      } else if (args[i] == _T("sharp")) {
        interpMode = INTERPOLATION_SHARP;
      } else {
        interpMode = INTERPOLATION_NONE;
      }
    } else if (args[i] == _T("--mute")) {
      if (solos.size()) {
        std::cerr << argv[0] << _T(": only one of --mute or --solo can be specified");
        return 1;
      }
      ++i;
      mutes.push_back(A_TO_D(args[i].c_str()));
    } else if (args[i] == _T("--solo")) {
      if (mutes.size()) {
        std::cerr << argv[0] << _T(": only one of --mute or --solo can be specified");
        return 1;
      }
      ++i;
      solos.push_back(A_TO_D(args[i].c_str()));
    } else if (args[i] == _T("--keep-silence")) {
      skipSilence = false;
    } else if (args[i] == _T("--preamp")) {
      ++i;
      preampFactor = A_TO_D(args[i].c_str());
    } else if (inputFilename.empty()) {
      inputFilename = args[i];
    } else if (outputFilename.empty()) {
      outputFilename = args[i];
    } else {
      unexpected = true;
    }
  }

  if (inputFilename.empty() || outputFilename.empty() || sampleRate <= 0 || unexpected) {
    std::cerr << _T("Usage: ") << argv[0] << _T(" [--rate N]  [--length SECONDS] [--speed MULT] [--tempo MULT]") << std::endl;
    std::cerr << _T("       [--solo CH]* [--mute CH]* [--keep-silence] [--preamp MULT] [--interp MODE] [--info] <input.ncsf> <output.wav>") << std::endl;
    std::cerr << _T("  --rate          Specifies the sample rate (default 32728)") << std::endl;
    std::cerr << _T("  --length        Stop decoding after the specified duration (default unlimited)") << std::endl;
    std::cerr << _T("  --speed         Adjust the playback speed by a factor of MULT (default 100%)") << std::endl;
    std::cerr << _T("  --tempo         Adjust the song tempo by a factor of MULT (default 100%)") << std::endl;
    std::cerr << _T("                  MULT may be specified as a number or a percentage.") << std::endl;
    std::cerr << _T("  --solo          Specifies a track to play solo. May be used multiple times.") << std::endl;
    std::cerr << _T("  --mute          Specifies a track to mute. May be used multiple times.") << std::endl;
    std::cerr << _T("                  --solo and --mute may not be used at the same time.") << std::endl;
    std::cerr << _T("  --keep-silence  Specifies not to skip leading silence when rendering.") << std::endl;
    std::cerr << _T("  --preamp        Adjust the sample volume by a factor of MULT before mixing (default 100%)") << std::endl;
    std::cerr << _T("  --info          Generates an HTML file describing the SDAT in addition to the output file") << std::endl;
    std::cerr << _T("  --interp        Specifies the interpolation mode (default none)") << std::endl;
    std::cerr << _T("    none            No interpolation") << std::endl;
    std::cerr << _T("    linear          Linear interpolation") << std::endl;
    std::cerr << _T("    l4              4-point Lagrange interpolation") << std::endl;
    std::cerr << _T("    l6              6-point Lagrange interpolation") << std::endl;
    std::cerr << _T("    sinc            Sinc interpolation") << std::endl;
    std::cerr << _T("    sharp           Sharp interpolation") << std::endl;
    std::cerr << _T("Sharp interpolation is a custom interpolation algorithm designed to") << std::endl;
    std::cerr << _T("preserve digital sounds such as sawtooth and square waves.") << std::endl;
    return 1;
  }

  XSFPlayer_NCSF player(inputFilename);
  player.SetSampleRate(sampleRate / speedFactor);
  player.SetInterpolation(interpMode);
  if (solos.size()) {
    std::bitset<16> muteBitset(0xFFFFULL);
    for (int ch : solos) {
      muteBitset[ch] = 0;
    }
    player.SetTrackMutes(muteBitset);
  } else if (mutes.size()) {
    std::bitset<16> muteBitset(0x0ULL);
    for (int ch : mutes) {
      muteBitset[ch] = 1;
    }
    player.SetTrackMutes(muteBitset);
  }
  bool ok = player.Load();
  if (!ok) {
    std::cerr << "failed to load" << std::endl;
    return 1;
  }
  player.SeekTop();
  player.SetTempoRate(tempoFactor);
  player.SetPreamp(preampFactor);

  if (dumpHtml) {
    DumpInfo info(&player);
    info.save(outputFilename + string(_T(".html")));
  }

#if !defined(_WIN32) || defined(__CYGWIN__)
  if (outputFilename == "-") {
    outputFilename = "/dev/stdout";
  }
#endif
  std::ofstream output(outputFilename.c_str(), std::ios::out | std::ios::binary);
  std::vector<uint8_t> SR = littleEndian(sampleRate), BR = littleEndian(sampleRate * 2 * 2);
  std::vector<uint8_t> riff({
    'R', 'I', 'F', 'F',         // Chunk ID
    0xFF, 0xFF, 0xFF, 0xFF,     // Chunk size: will fill in afterward
    'W', 'A', 'V', 'E',         // Format
    'f', 'm', 't', ' ',         // Subchunk 1 ID
    16, 0, 0, 0,                // Subchunk 1 size
    1, 0,                       // Audio format (linear PCM)
    2, 0,                       // Number of channels (stereo)
    SR[0], SR[1], SR[2], SR[3], // Sample rate
    BR[0], BR[1], BR[2], BR[3], // Byte rate (sample rate * 2 bytes/sample * 2 channels)
    4, 0,                       // Block alignment (2 bytes/sample * 2 channels)
    16, 0,                      // Bits per sample
    'd', 'a', 't', 'a',         // Subchunk 2 ID
    0xFF, 0xFF, 0xFF, 0xFF      // Subchunk 2 size: will fill in afterward
  });
  output.write(reinterpret_cast<char*>(riff.data()), riff.size());

  std::cerr << "Decoding \"" << inputFilename << "\" to \"" << outputFilename << "\"..." << std::endl;
  std::vector<uint8_t> buf(sampleRate * 4);
  std::cerr << "Progress: 0 sec" << std::flush;
  unsigned int samplesWritten = 0;
  unsigned int bytesWritten = 0;
  int seconds = 0;
  bool more = true;
  unsigned int samplesPerStep = sampleRate;
  if (skipSilence) {
    samplesPerStep = sampleRate / 256;
  }
  while (more && (!limitSeconds || limitSeconds > seconds)) {
    for (unsigned int i = 0; i < sampleRate; i += samplesPerStep) {
      samplesWritten = 0;
      player.FillBuffer(buf, samplesWritten);
      if (samplesWritten < samplesPerStep) {
        more = false;
        break;
      }
      if (skipSilence) {
        for (unsigned int j = 0; j < samplesWritten * 4; j += 4) {
          if (buf[j] != 0 || buf[j+1] != 0 || buf[j+2] != 0 || buf[j+3] != 0) {
            skipSilence = false;
            samplesPerStep = sampleRate;
            break;
          }
        }
        if (skipSilence) {
          continue;
        }
      }
      output.write(reinterpret_cast<char*>(buf.data()), (samplesWritten * 4));
      bytesWritten += samplesWritten * 4;
    }
    seconds = bytesWritten / (sampleRate * 4);
    std::cerr << "\rProgress: " << seconds << " sec" << std::flush;
  };
  std::cerr << std::endl;

  output.seekp(40, std::ios::beg);
  if (output.fail()) {
    std::cerr << "\nWarning: output is not seekable, WAVE header will have incorrect length" << std::endl;
  } else {
    // stream is seekable
    // if it's not, then we just have to let the other end of the pipe deal with it
    std::vector<uint8_t> sizeBytes({
      uint8_t((bytesWritten) & 0xFF),
      uint8_t((bytesWritten >> 8) & 0xFF),
      uint8_t((bytesWritten >> 16) & 0xFF),
      uint8_t(bytesWritten >> 24)
    });
    output.write(reinterpret_cast<char*>(sizeBytes.data()), 4);

    bytesWritten += 36;
    sizeBytes = std::vector<uint8_t>({
      uint8_t((bytesWritten) & 0xFF),
      uint8_t((bytesWritten >> 8) & 0xFF),
      uint8_t((bytesWritten >> 16) & 0xFF),
      uint8_t(bytesWritten >> 24)
    });
    output.seekp(4, std::ios::beg);
    output.write(reinterpret_cast<char*>(sizeBytes.data()), 4);
  }
  output.close();

  std::cerr << "Done! Wrote " << bytesWritten << " bytes." << std::endl;

  return 0;
}
