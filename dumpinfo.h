#ifndef N2W_DUMPINFO_H
#define N2W_DUMPINFO_H

#include <string>
class XSFPlayer_NCSF;

#ifdef _WIN32
using string = std::wstring;
#else
using string = std::string;
#endif

class DumpInfo {
public:
  DumpInfo(XSFPlayer_NCSF* ncsf);

  void save(const string& filename);

private:
  XSFPlayer_NCSF* ncsf;
};

#endif
