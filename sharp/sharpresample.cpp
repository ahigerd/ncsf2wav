#include <iostream>
#include <fstream>
#include <climits>
#include <cstdlib>
#include "sharp.h"

int32_t read16(std::istream& input) {
  unsigned char low = input.get();
  unsigned char high = input.get();
  if (!input) {
    return 0;
  } else if (high & 0x80) {
    return low | (high << 8) | 0xFFFF0000;
  } else {
    return low | (high << 8);
  }
}

uint32_t read32(std::istream& input) {
  uint8_t buf[4];
  input.read(reinterpret_cast<char*>(buf), 4);
  if (!input) {
    return 0;
  }
  return buf[0] | (buf[1] << 8) | (buf[2] << 16) | (buf[3] << 24);
}

void write16(std::ostream& output, int32_t value) {
  if (value < -0x8000) {
    value = -0x8000;
  } else if (value > 0x7FFF) {
    value = 0x7FFF;
  }
  uint32_t v = value;
  output.put(v & 0xFF);
  output.put((v & 0xFF00) >> 8);
}

std::vector<uint8_t> littleEndian(uint32_t value) {
  return std::vector<uint8_t>({
    uint8_t(value),
    uint8_t(value >> 8),
    uint8_t(value >> 16),
    uint8_t(value >> 24)
  });
}

int main(int argc, char** argv) {
  if (argc < 2) {
    std::cerr << "Usage: " << argv[0] << " <input> [<samplerate>]" << std::endl;
    return 1;
  }
  int32_t targetRate = 44100;
  if (argc >= 3) {
    targetRate = std::atoi(argv[2]);
    if (targetRate < 1) {
      std::cerr << argv[0] << ": invalid sample rate";
      return 1;
    }
  }
  std::ifstream input(argv[1], std::ios::binary);
  if (!input.good()) {
    std::cerr << argv[0] << ": unable to read " << argv[1] << std::endl;
    return 1;
  }
  int magic = read32(input);
  if (magic != 0x46464952) {
    std::cerr << argv[0] << ": " << argv[1] << " is not a valid WAVE file" << std::endl;
    return 1;
  }
  input.seekg(22, std::ios::beg);
  int channels = read16(input);
  if (!input.good() || (channels != 1 && channels != 2)) {
    std::cerr << argv[0] << ": " << argv[1] << " is not a valid WAVE file" << std::endl;
    return 1;
  }
  int32_t inputRate = read32(input);
  if (!input.good() || inputRate < 1) {
    std::cerr << argv[0] << ": could not get sample rate" << std::endl;
    return 1;
  }
  input.seekg(36, std::ios::beg);
  magic = read32(input);
  while (magic != 0x61746164 && input.good()) {
    uint32_t skip = read32(input);
    input.ignore(skip);
    magic = read32(input);
  }
  uint32_t inputSize = read32(input);
  double ratio = (double)targetRate / (double)inputRate;
  if (!input.good() || inputSize < 4 || inputSize > INT_MAX / ratio) {
    std::cerr << argv[0] << ": " << argv[1] << " is not a valid WAVE file" << std::endl;
    return 1;
  }
  uint32_t outputSize = inputSize * ratio;
  uint32_t inputSamples = inputSize / (channels * 2);
  // TODO: handle 8-bit wave files
  SharpInterpolator left(inputRate, targetRate);
  SharpInterpolator right(inputRate, targetRate);
  std::vector<uint8_t>
    CS = littleEndian(outputSize + 36),
    SS = littleEndian(outputSize),
    SR = littleEndian(targetRate),
    BR = littleEndian(targetRate * 2 * 2);
  std::vector<uint8_t> riff({
    'R', 'I', 'F', 'F',         // Chunk ID
    CS[0], CS[1], CS[2], CS[3], // Chunk size
    'W', 'A', 'V', 'E',         // Format
    'f', 'm', 't', ' ',         // Subchunk 1 ID
    16, 0, 0, 0,                // Subchunk 1 size
    1, 0,                       // Audio format (linear PCM)
    2, 0,                       // Number of channels (stereo)
    SR[0], SR[1], SR[2], SR[3], // Sample rate
    BR[0], BR[1], BR[2], BR[3], // Byte rate (sample rate * 2 bytes/sample * 2 channels)
    4, 0,                       // Block alignment (2 bytes/sample * 2 channels)
    16, 0,                      // Bits per sample
    'd', 'a', 't', 'a',         // Subchunk 2 ID
    SS[0], SS[1], SS[2], SS[3]  // Subchunk 2 size:
  });
  std::cout.write(reinterpret_cast<char*>(riff.data()), riff.size());
  int32_t lastLeft = read16(input);
  int32_t lastRight = (channels == 2) ? read16(input) : lastLeft;
  uint32_t inputPosition = 0;
  double outputPosition = 0;
  double stepSize = 1.0 / ratio;
  std::vector<int32_t> leftBuffer{ 0, 0, 0, 0, 0}, rightBuffer{0, 0, 0, 0, 0};
  while (input.good() && inputPosition < inputSamples) {
    int32_t leftSample = read16(input);
    int32_t rightSample = (channels == 2) ? read16(input) : leftSample;
    leftBuffer.erase(leftBuffer.begin());
    leftBuffer.push_back(leftSample);
    rightBuffer.erase(rightBuffer.begin());
    rightBuffer.push_back(rightSample);
    while ((uint32_t)outputPosition == inputPosition) {
      int32_t leftOut = left.nextSample(outputPosition, 1, leftBuffer.data());
      int32_t rightOut = right.nextSample(outputPosition, 1, rightBuffer.data());
      write16(std::cout, leftOut);
      write16(std::cout, rightOut);
      outputPosition += stepSize;
    }
    lastLeft = leftSample;
    lastRight = rightSample;
    inputPosition += 1;
  }
  while (outputPosition < inputSamples) {
    int32_t leftOut = left.nextSample(outputPosition, lastLeft, 0);
    int32_t rightOut = right.nextSample(outputPosition, lastRight, 0);
    write16(std::cout, leftOut);
    write16(std::cout, rightOut);
    outputPosition += stepSize;
  }
  return 0;
}
