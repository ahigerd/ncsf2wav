#ifndef SHARP_MATRICKERY_H
#define SHARP_MATRICKERY_H

#include <array>

template <int DIM>
struct PolyEq {
  std::array<double, DIM> vec;

  PolyEq(const std::array<double, DIM>& v) : vec(v) {}
  PolyEq(std::array<double, DIM>&& v) : vec(v) {}
  PolyEq(const PolyEq<DIM>& other) = default;
  PolyEq(PolyEq<DIM>&& other) = default;

  inline double operator()(double x) const {
    double result = vec[0];
    for (int i = 1; i < DIM; i++) {
      result = (x * result) + vec[i];
    }
    return result;
  }
};

template <int DIM>
class EqSystem {
public:
  std::array<std::array<double, DIM>, DIM + 1> matrix;

  static EqSystem fromPoints(double p[][2]) {
    EqSystem<DIM> result;
    for (int i = 0, k = 0; i < DIM; i++) {
      result.matrix[DIM - 1][i] = 1;
      if (p[i][0] == 0) {
        result.matrix[DIM][DIM - 1] = p[i][1];
        for (int j = 0; j < DIM - 1; j++) {
          result.matrix[j][DIM - 1] = 0;
        }
      } else {
        result.matrix[DIM][k] = p[k][1];
        for (int j = DIM - 2; j >= 0; j--) {
          result.matrix[j][k] = result.matrix[j + 1][k] * p[k][0];
        }
        k++;
      }
    }
    return result;
  }
  
  PolyEq<DIM> solve() {
    for (int y = 0; y < DIM; y++) {
      for (int x = DIM; x >= y; x--) {
        matrix[x][y] /= matrix[y][y];
      }
      for (int i = 0; i < DIM; i++) {
        if (i == y) continue;
        double mult = matrix[y][i];
        if (mult == 0) continue;
        for (int x = y; x <= DIM; x++) {
          matrix[x][i] -= matrix[x][y] * mult;
        }
      }
    }
    return PolyEq<DIM>(matrix[DIM]);
  }
};

template <int DIM>
inline EqSystem<DIM> EqSystemFromPoints(double(&p)[DIM][2]) {
  return EqSystem<DIM>::fromPoints(p); 
}

#endif
