#ifndef SHARP_INTERPOLATION_H
#define SHARP_INTERPOLATION_H

/*
 * Sharp interpolation routine
 * By Adam Higerd (Coda Highland) <chighland@gmail.com>
 */
#include <stdint.h>
#include <vector>

struct SharpInterpolator {
private:
  double resampleRatio;
  double inputLoudness, outputLoudness;

public:
  SharpInterpolator(double resampleRatio);
  SharpInterpolator(uint32_t inputRate, uint32_t outputRate);

  void reset();
  int32_t nextSample(double outputPosition, double sampleDuration, int32_t samples[5]);
  std::vector<int32_t> resample(const std::vector<int32_t>& input);
};

#endif

