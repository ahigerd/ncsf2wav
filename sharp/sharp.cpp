#include "sharp.h"
#include "matrickery.h"
#include <cmath>
#include <iostream>

#define SAW_THRESHOLD 0x1000

SharpInterpolator::SharpInterpolator(double resampleRatio)
: resampleRatio(resampleRatio)
{
  reset();
}

SharpInterpolator::SharpInterpolator(uint32_t inputRate, uint32_t outputRate)
: SharpInterpolator((double)outputRate / (double)inputRate)
{
  // Forwarded constructor only
}

void SharpInterpolator::reset()
{
  inputLoudness = 0;
  outputLoudness = 0;
}

int32_t SharpInterpolator::nextSample(double outputPosition, double sampleDuration, int32_t samples[5]) {
  int32_t sample;
  bool isExtremum = (samples[2] > samples[1]) == (samples[2] > samples[3]);
  double sampleStart = std::floor(outputPosition);
  double ix = 0; // by default, treat the current input sample as occurring on the boundary
  bool skipLeft = false, skipRight = false;
  if (isExtremum) {
    int subsample = (outputPosition - sampleStart) / sampleDuration;
    int denom = samples[0] - (2.0 * samples[1]) + samples[2];
    ix = denom ? (samples[2] - samples[1]) / denom : 0;
    int peakSubsample = std::floor(ix / sampleDuration);
    if (peakSubsample == subsample) {
      inputLoudness = (inputLoudness + std::abs(samples[1])) * .5;
      outputLoudness = (outputLoudness + std::abs(sample)) * .5;
      return samples[1];
    }
    skipLeft = peakSubsample < subsample;
    skipRight = peakSubsample > subsample;
  } else {
    int32_t steepIn = std::abs(samples[0] - samples[1]);
    int32_t steepOut = std::abs(samples[1] - samples[2]);
    skipLeft = steepIn > SAW_THRESHOLD;
    skipRight = steepOut > SAW_THRESHOLD;
  }
  if (skipLeft && !skipRight) {
    double points[3][2]{
      { sampleStart + ix, samples[1] * 1.0 },
      { sampleStart + sampleDuration, samples[2] * 1.0 },
      { sampleStart + sampleDuration * 2, samples[3] * 1.0 },
    };
    auto poly = EqSystemFromPoints(points).solve();
    sample = poly(outputPosition);
  } else if (skipRight && !skipLeft) {
    double points[3][2]{
      { sampleStart - sampleDuration * 2, samples[-1] * 1.0 },
      { sampleStart - sampleDuration, samples[0] * 1.0 },
      { sampleStart + ix, samples[1] * 1.0 },
    };
    auto poly = EqSystemFromPoints(points).solve();
    sample = poly(outputPosition);
  } else {
    double points[5][2]{
      { sampleStart - sampleDuration * 2, samples[-1] * 1.0 },
      { sampleStart - sampleDuration, samples[0] * 1.0 },
      { sampleStart + ix, samples[1] * 1.0 },
      { sampleStart + sampleDuration, samples[2] * 1.0 },
      { sampleStart + sampleDuration * 2, samples[3] * 1.0 },
    };
    auto poly = EqSystemFromPoints(points).solve();
    sample = poly(outputPosition);
  }

  // clamp to prevent clipping or distortion
  int min = (samples[1] < samples[2]) ? samples[1] : samples[2];
  int max = (samples[1] > samples[2]) ? samples[1] : samples[2];
  if (sample < min) {
    sample = min;
  } else if (sample > max) {
    sample = max;
  }

  // apply gain control to avoid systematic errors
  inputLoudness = (inputLoudness + std::abs(samples[1])) * .5;
  double newOutputLoudness = (outputLoudness + std::abs(sample)) * .5;
  if (inputLoudness > 0) {
    double gain = newOutputLoudness / inputLoudness;
    if (gain > 1) {
      double ratio = std::log10(gain);
      if (ratio > 1) ratio = 1;
      int32_t offset = (sample > 0 ? sample - min : sample - max);
      sample -= offset * ratio;
      newOutputLoudness = (outputLoudness + std::abs(sample)) * .5;
    }
  }
  outputLoudness = newOutputLoudness;

  return sample;
}

std::vector<int32_t> SharpInterpolator::resample(const std::vector<int32_t>& input) {
  int inputLength = input.size();
  int outputLength = std::floor(inputLength * resampleRatio);
  std::vector<int32_t> output(outputLength);
  double outputPosition = 0;
  double stepSize = 1.0 / resampleRatio;
  int outputIndex = 0;
  int32_t samples[4] = { 0, input[0], input[1], input[2] };
  output[outputIndex] = nextSample(outputPosition, resampleRatio, samples);
  outputIndex += 1;
  outputPosition += stepSize;
  for (int i = 1; i < inputLength - 3; i++) {
    while ((uint32_t)outputPosition == i) {
      for (int j = 0; j < 4; j++) samples[j] = input[i - 1 + j];
      output[outputIndex] = nextSample(outputPosition, resampleRatio, samples);
      outputIndex += 1;
      outputPosition += stepSize;
    }
  }
  while (outputIndex < outputLength) {
    for (int j = 0; j < 3; j++) samples[j] = samples[j + 1];
    samples[3] = 0;
    output[outputIndex] = nextSample(outputPosition, resampleRatio, samples);
    outputIndex += 1;
    outputPosition += stepSize;
  }
  return output;
}

