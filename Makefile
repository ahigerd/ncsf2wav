ifeq ($(OS),Windows_NT)
	WINDOWS := 1
ifeq ($(findstring ;,$(PATH)),;)
	CYGWIN := 0
else ifeq ($(shell uname -o),Cygwin)
	CYGWIN := 1
else
	CYGWIN := 0
endif
else ifneq ($(findstring mingw,$(CXX)),)
	WINDOWS := 1
	CYGWIN := 0
ifeq ($(findstring mingw,$(CC)),)
	CC := $(subst g++,gcc,$(CXX))
endif
else
	WINDOWS := 0
	CYGWIN := 0
endif

ifeq ($(WINDOWS),1)
all: ncsf2wav.exe
CXXFLAGS += -D_WINDOWS -mmmx -msse -msse2 -isystem zlib
CFLAGS += -D_WINDOWS -mmmx -msse -msse2
ifeq ($(CYGWIN),1)
	LDFLAGS += -lz
else
	LDFLAGS += -municode
endif
else
CXXFLAGS += $(shell pkg-config --cflags zlib)
LDFLAGS += $(shell pkg-config --libs zlib)
all: ncsf2wav
endif

HEADERS = $(wildcard sharp/*.h sseqplayer/*.h *.h)
SOURCES = $(wildcard sharp/sharp.cpp sseqplayer/*.cpp *.cpp)
OBJECTS = $(patsubst %.cpp, %.o, $(SOURCES))
ZLIB_SRCS = $(wildcard zlib/*.c)
ZLIB_OBJS = $(patsubst %.c, %.o, $(ZLIB_SRCS))

%.o: %.c
	$(CC) -O3 $(CFLAGS) -c -o $@ $<

%.o: %.cpp $(HEADERS) Makefile
	$(CXX) -std=gnu++14 -O3 $(CXXFLAGS) -c -I. -Isharp/ -Isseqplayer/ -fpermissive -o $@ $<

ncsf2wav: $(OBJECTS)
	$(CXX) -o $@ $^ $(LDFLAGS)
ifneq ($(STRIP),)
	$(STRIP) $@
endif

ncsf2wav.exe: $(OBJECTS) $(ZLIB_OBJS)
	$(CXX) -o $@ -static -static-libstdc++ -static-libgcc $^ $(LDFLAGS)
ifneq ($(STRIP),)
	$(STRIP) $@
endif

clean:
	rm -f ncsf2wav ncsf2wav.exe *.o sseqplayer/*.o sharp/*.o
