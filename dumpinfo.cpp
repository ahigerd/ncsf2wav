#include "dumpinfo.h"
#include "sseqplayer/XSFPlayer_NCSF.h"
#include <fstream>
#include <sstream>

static const char* noteNames[] = { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };
static const char* waveTypes[] = { "PCM8", "PCM16", "ADPCM" };
static const int offsetSize[] = { 4, 2, 8 };

static double timerToRate(int timer) {
  return 33513982 / 2 / (timer);
}
static int rateToTimer(double hz) {
  return 33513982.0 / hz / 2;
}

std::string noteName(uint8_t note)
{
  std::stringstream ss;
  ss << noteNames[note % 12] << (int(note / 12) - 1);
  return ss.str();
}

DumpInfo::DumpInfo(XSFPlayer_NCSF* ncsf) : ncsf(ncsf)
{
  // initializers only
}

void DumpInfo::save(const string& filename)
{
  std::ofstream out(filename.c_str(), std::ios::out);
  const XSFFile* xsf = ncsf->GetXSFFile();
  out << "<html><head><title>" << xsf->GetFilename() << "</title></head><body><h1>" << xsf->GetFilename() << "</h1>" << std::endl << std::endl;
  SDAT* sdat = ncsf->GetSDAT();
  SBNK* sbnk = sdat->sbnk.get();
  int i = 0;
  out << "<h2>Instruments</h2>" << std::endl << std::endl;
  for (const SBNKInstrument& inst : sbnk->instruments) {
    out << "<h3>Instrument " << i << "<h3>" << std::endl;
    out << "<table border='1'><tr><th>Range</th><th>Sample</th><th>Base Note</th><th>Pan</th><th>Attack</th><th>Decay</th><th>Sustain</th><th>Release</th></tr>" << std::endl;
    for (const SBNKInstrumentRange& range : inst.ranges) {
      out << "<tr><td>" << noteName(range.lowNote);
      if (range.lowNote != range.highNote) {
        out << " - " << noteName(range.highNote);
      }
      out << "</td><td>";
      if (inst.record == 2) {
        out << "Square " << ((range.swav & 0x7) * 12.5) << "%";
      } else if (inst.record == 3) {
        out << "Noise";
      } else {
        out << "<a href='#s" << range.swar << "_" << range.swav << "'>" << range.swar << "::" << range.swav << "</a>";
      }
      out << "</td>";
      out << "<td>" << noteName(range.noteNumber) << "</td>";
      out << "<td>" << int(range.pan) << "</td>";
      out << "<td>" << int(range.attackRate) << "</td>";
      out << "<td>" << int(range.decayRate) << "</td>";
      out << "<td>" << int(range.sustainLevel) << "</td>";
      out << "<td>" << int(range.releaseRate) << "</td>";
      out << "</tr>" << std::endl;
    }
    out << "</table>" << std::endl << std::endl;
    i++;
  }
  out << "<h2>Samples</h2>" << std::endl << std::endl;
  out << "<table border='1'><tr><th>Sample</th><th>Type</th><th>Sample Rate</th><th>Timer</th><th>Length</th><th>Loop Start</th>";
  for (int sIdx = 0; sIdx < 4; sIdx++) {
    SWAR* swar = sdat->swar[sIdx].get();
    if (!swar) continue;
    for (const auto& iter : swar->swavs) {
      const SWAV* swav = &iter.second;
      out << "<tr><td><a name='s" << sIdx << "_" << iter.first << "'>" << sIdx << "::" << iter.first << "</a></td>";
      out << "<td>" << waveTypes[swav->waveType & 3];
      if (swav->loop) out << " (looping)";
      out << "</td>";
      double rateFromTimer = timerToRate(swav->time);
      int timerFromRate = rateToTimer(swav->sampleRate);
      if (swav->time != timerFromRate) {
        out << "<td><b>" << swav->sampleRate << " (" << rateFromTimer << ")</b></td>";
        out << "<td><b>0x" << std::hex << swav->time << " (0x" << std::hex << rateToTimer(swav->sampleRate) << std::dec << ")</b></td>";
      } else {
        out << "<td>" << swav->sampleRate << " (" << rateFromTimer << ")</td><td>0x" << std::hex << swav->time << std::dec << "</td>";
      }
      out << "<td>" << swav->nonLoopLength << "</td>";
      if (swav->loop) {
        out << "<td>" << swav->loopOffset << "</td>";
      } else {
        out << "<td>&nbsp;</td>";
      }
      out << "</tr>";
    }
  }
  out << "</table></body></html>";
}
